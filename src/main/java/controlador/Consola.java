/*
 * Copyright (C) 2021 Jhon Alexander Buitrago Gonzalez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package controlador;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 *
 * @author Jhon Alexander Buitrago Gonzalez
 */
@Data
@AllArgsConstructor
public class Consola {
    
    private javax.swing.JTextArea consolaLog;
    private javax.swing.JProgressBar barraProceso;
    
    /**
     * Método para manejar la consola de la aplicacion donde se muestra informacion al usuario de lo que hace la aplicación.
     * @param datos Es el texto que sera impreso en la ventana de consola.
     * @param carga Contiene un valor numerico para ser mostrado en la barra de progreso.
     */
    public void cargeConsola(String datos, int carga) {
        consolaLog.append(datos + "\n");
        barraProceso.setValue(carga);
    }
}
