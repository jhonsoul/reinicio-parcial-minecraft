/*
 * Copyright (C) 2021 Jhon Alexander Buitrago Gonzalez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package controlador;

import java.io.File;

/**
 *
 * @author Jhon Alexander Buitrago Gonzalez
 */
public class VerificadorExistencia {
    
    /**
     * Método que sirve para verificar si el juego esta instalado en el sistema.
     * @param ruta Dato que contiene la ruta donde esta instalado el juego.
     * @return Regresa un verdadero si el juego esta instalado.
     */
    public boolean juegoInstalado(String ruta) {
        boolean existe = false;
        if (new File(ruta).exists()) existe = true;
        return existe;
    }
    
    /**
     * Método para verificar si existe el archivo con la lista de regiones que no se borraran.
     * @param ruta contiene la ruta de la del archivo de regiones.
     * @return Regresa verdadero si existe el archivo de region.
     */
    public boolean existeArchivoRegiones(String ruta) {
        boolean existe = false;
        if (new File(ruta).exists()) existe = true;
        return existe;
    }
    
}
