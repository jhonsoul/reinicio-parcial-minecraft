/*
 * Copyright (C) 2021 Jhon Alexander Buitrago Gonzalez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package controlador;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Clase que se encarga entregar datos necesario para hacer la eliminación de las regiones.
 * @author Jhon Alexander Buitrago Gonzalez
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProcesoRegiones {
    
    private List<String> archivosPC;
    private List<String> documentoRegiones;
    
    
    /**
     * Método para entregar la cantidad de regiones posibles que se pueden eliminar.
     * @return Entrega un numero entero de la cantidad de regiones a eliminar.
     */
    public int numeroRegionesEliminar() {
        documentoRegiones.forEach((t) -> {
            archivosPC.remove(t);
        });
        return archivosPC.size();
    }
    
    /**
     * Método que entrega la lista de regiones que se pueden eliminar.
     * @return La lista que contiene los nombres de las regiones que pueden ser eliminadas.
     */
    public List<String> listaRegionesEliminar() {
        documentoRegiones.forEach((t) -> {
            archivosPC.remove(t);
        });
        return archivosPC;
    }
    
    /**
     * Metodo que traduce el nombre de la region con la ruta en el computador.
     * @param nombre Este parametro contiene el nombre de la region para ser filtrada.
     * @return Entrega la ubicación de la dimensión solicitada.
     */
    public String urlRegion(String nombre) {
        Map<String, String> regiones = new HashMap<>();
        regiones.put("Mapa principal", "region");
        regiones.put("End", "DIM1\\region");
        regiones.put("Nether", "DIM-1\\region");
        return regiones.get(nombre);
    }
}
