/*
 * Copyright (C) 2021 Jhon Alexander Buitrago Gonzalez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package controlador;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Jhon Alexander Buitrago Gonzalez
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RutasMapeado {
    private final String RUTA_PRINCIPAL = System.getenv("APPDATA") + "\\.minecraft\\saves\\";
    private String rutaMapa;
    private String dimension;
    private String NOMBRE_ARCHIVO_REGIONES = "regions.txt";
    private String rutaArchivoRegiones;

    public void setRutaMapa(String rutaMapa) {
        this.rutaMapa = RUTA_PRINCIPAL + rutaMapa + "\\";
    }
    
    /**
     * Método para establecher la ruta de la dimensión que sera modificada.
     * @param dimension Parametro necesario para establecer la ruta de la dimensión.
     */
    public void setDimension(String dimension) {
        if (!rutaMapa.isEmpty() || rutaMapa != null) {
            this.dimension = rutaMapa + dimension + "\\";
            rutaArchivoRegiones = this.dimension + NOMBRE_ARCHIVO_REGIONES;
        }
    }
    
    /**
     * Establece la ruta del archivo de dimensiones para ser leído.
     */
    public void setRutaArchivoRegiones() {
        if (!dimension.isEmpty() || dimension != null) {
            rutaArchivoRegiones = dimension + NOMBRE_ARCHIVO_REGIONES;
        }
    }
}
