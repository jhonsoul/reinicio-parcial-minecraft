/*
 * Copyright (C) 2021 Jhon Alexander Buitrago Gonzalez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package almacenamiento;

import controlador.RutasMapeado;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jhon Alexander Buitrago Gonzalez
 */


public class LecturaInformacion extends RutasMapeado{
    
    /**
     * Metódo que se encarga de leer las carpetas que contiene los mapas guardados.
     * @return Entrega una lista con los nombres de los mapas guardados.
     */
    public List listaMapas() {
        File[] archivos = new File(getRUTA_PRINCIPAL()).listFiles();
        List<String> listaMapasPC = new LinkedList();
        for (File archivo : archivos) {
            if (archivo.isDirectory()) {
                listaMapasPC.add(archivo.getName());
            }
        }
        return listaMapasPC;
    }
    
    /**
     * Se encarga de leer los archivos de región que se encuentra en el computador.
     * @return Llena una lista con la ubicación de los archivos del computador.
     */
    public List leerRegionesPC() {
        File[] archivos = new File(getDimension()).listFiles();
        List<String> listaArchivosPC = new LinkedList();
        for (File archivo : archivos) {
            if (archivo.isFile() && archivo.toString().endsWith(".mca")) {
                listaArchivosPC.add(archivo.getName());
            }
        }
        return listaArchivosPC;
    }
    
    /**
     * Método para leer el archivo txt que contiene una lista de los nombres de los archivos que no se borraran.
     * @return Entrega la lista de nombres de los archivos que no seran borrados.
     */
    public List leerArchivo() {
        try {
            String contenidoArchivo = Files.readString(Path.of(getRutaArchivoRegiones()));
            List<String> listaContenidoArchivos = Arrays.asList(contenidoArchivo.split("\\r?\\n"));
            return listaContenidoArchivos;
        } catch (IOException ex) {
            Logger.getLogger(ManejoArchivos.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
}
