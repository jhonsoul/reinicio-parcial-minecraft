/*
 * Copyright (C) 2021 Jhon Alexander Buitrago Gonzalez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package almacenamiento;

import controlador.RutasMapeado;
import controlador.Consola;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jhon Alexander Buitrago Gonzalez
 */

public class ManejoArchivos extends RutasMapeado{
      
    /**
     * Método para crear el archivo que contendra los nombres de las regiones del usuario que tiene construccion y desea conservar.
     * @param listaRegiones Contiene los nombres de las regiones para que no sean borradas.
     * @param ruta Es la ruta base donde esta los archivos de region.
     * @return Devuelve un estado positivo si no se presenta algun fallo.
     */
    public boolean crearArchivoRegion(List<String> listaRegiones, String ruta) {
        String contenido = getNOMBRE_ARCHIVO_REGIONES();
        contenido = listaRegiones.stream().map(listaRegion -> System.lineSeparator() + listaRegion).reduce(contenido, String::concat);
        try {
            Files.writeString(Path.of(ruta), contenido, StandardOpenOption.CREATE_NEW);
            return true;
        } catch (IOException ex) {
            Logger.getLogger(ManejoArchivos.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    /**
     * Método para eliminar las regiones visitadas
     * @param rutaCarpeta Variable con la ruta donde se procedera el borrado de las regiones.
     * @param listaArchivos Lista con el nombre de archivos que seran borrados.
     * @param cantidadArchivos Entrega el numero de archivos que seran borrados.
     * @param consolaLog Objeto de consola para mostrar mensajes de informacion al usuario.
     * @return Devuelve si falla o no este proceso con un verdadero o un falso.
     */
    public boolean eliminarRegiones(String rutaCarpeta, List<String> listaArchivos, int cantidadArchivos, Consola consolaLog) {
        int porcentajeProgreso = 70 / cantidadArchivos;
        int progreso = 30;
        try {
            for (String listaArchivo : listaArchivos) {
                consolaLog.cargeConsola("Se encontro: " + listaArchivo + " para ser eliminado...", progreso);
                Files.delete(Path.of(rutaCarpeta + listaArchivo));
                progreso += porcentajeProgreso;
                consolaLog.cargeConsola("El archivo "+ listaArchivo + " fue eliminado...", progreso);
            }
            return true;
        } catch (IOException ex) {
            Logger.getLogger(ManejoArchivos.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
}
